package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

const (
	Ipv4Url  = "http://whatismyip.akamai.com"
	Ipv6Url  = "http://ipv6.whatismyip.akamai.com"
	Ipv4Type = "A"
	Ipv6Type = "AAAA"
)

func GetCurrentIPv4() (string, error) {
	var ip string
	var e error
	if configuration.IPUrl != "" {
		ip, e = GetIpOnline(configuration.IPUrl)
	} else {
		ip, e = GetIpOnline(Ipv4Url)
	}

	if e != nil {
		log.Println("Cannot get IPv4...")
		return "", e
	}
	return ip, nil
}

func GetCurrentIPv6() (string, error) {
	var ip string
	var e error
	if configuration.IPV6Url != "" {
		ip, e = GetIpOnline(configuration.IPV6Url)
	} else {
		ip, e = GetIpOnline(Ipv6Url)
	}
	if e != nil {
		log.Println("Cannot get IPv6...")
		return "", e
	}
	return ip, nil
}

func GetIpOnline(ipUrl string) (string, error) {
	client := &http.Client{}
	response, err := client.Get(ipUrl)

	if err != nil {
		return "", err
	}
	defer response.Body.Close()
	body, _ := ioutil.ReadAll(response.Body)
	return strings.Trim(string(body), "\n"), nil
}

func CheckSettings(config *Settings) error {
	return nil
}
